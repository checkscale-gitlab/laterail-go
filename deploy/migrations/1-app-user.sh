#!/bin/bash
set -e

mongo <<EOF
use laterail
db.createUser({
  user:  '$APP_MONGO_USER',
  pwd: '$APP_MONGO_PASS',
  roles: ['readWrite']
})
EOF
