# Deploy pipeline: Kustomize and deploy ArgoCD ApplicationSet, restore any persistent cluster resources (e.g. TLS certificates)

variables:
  kube_config_test: $kube_config_test
  kube_config_prod: $kube_config_prod

stages:
  - kustomize
  - demo
  - apply
  - empty_cache

image:
  name: bash:latest

cache:
  key: ${CI_PROJECT_NAME}-${PARENT_STAGE}-k8s
  paths:
    - deploy/k8s/root.yaml

# Toggle kustomize environment stage and container image tags in ArgoCD ApplicationSet
kustomize:
  stage: kustomize
  script:
    - apk add --update jq
    - CONTAINER_TAG_WEB=$(jq -r .version src/react/package.json)
    - CONTAINER_TAG_API=$(cat src/api/version)
    - sed -i "s|REPLACE_STAGE|${PARENT_STAGE}|g" deploy/k8s/root.yaml
    - sed -i "s|REPLACE_REF|${PARENT_REF}|g" deploy/k8s/root.yaml
    - sed -i "s|REPLACE_API|${CONTAINER_TAG_API}|g" deploy/k8s/root.yaml
    - sed -i "s|REPLACE_WEB|${CONTAINER_TAG_WEB}|g" deploy/k8s/root.yaml
    - cat deploy/k8s/root.yaml

# Apply the TXT record to allow external-dns to manage PROD url
# Which is shared by DEMO static site
demo:
  stage: demo
  script:
    - apk add --update curl jq
    - ZONE_ID=f7970263abd9f52dd7d7d18f5b5be3fc
    - RECORD_NAME=laterail.com
    - TXT='\"heritage=external-dns,external-dns/owner=default,external-dns/resource=ingress/laterail/web-ingress\"'
    - |-
      set -e
      EXISTING=$(curl -X GET "https://api.cloudflare.com/client/v4/zones/${ZONE_ID}/dns_records?name=${RECORD_NAME}&type=TXT" \
        -H "Content-Type:application/json" \
        -H "Authorization: Bearer ${CLOUDFLARE_TOKEN}" | jq -r .result)
      if [[ $EXISTING != '[]' ]]
      then
        echo -e "Deleting existing matching TXT records: ${EXISTING}\n"
        for record in $(echo $EXISTING | jq -r '.[] | @base64')
        do
          ID=$(echo $record | base64 -d | jq -r .id)
          curl -X DELETE "https://api.cloudflare.com/client/v4/zones/${ZONE_ID}/dns_records/${ID}" \
            -H "Content-Type:application/json" \
            -H "Authorization: Bearer ${CLOUDFLARE_TOKEN}"
          echo -e "\n"
        done
      fi
        echo -e "Creating new TXT record for ${RECORD_NAME}\n"
        curl -X POST "https://api.cloudflare.com/client/v4/zones/${ZONE_ID}/dns_records" \
          -H "Content-Type:application/json" \
          -H "Authorization: Bearer ${CLOUDFLARE_TOKEN}" \
          --data '{"type":"TXT","name":"'${RECORD_NAME}'","content":"'${TXT}'","ttl":1,"priority":10,"proxied":false}'
        echo -e "\n"
  rules:
    - if: $PARENT_STAGE != 'prod'
      when: never
    - if: $PARENT_STAGE == 'prod'
      when: always
      allow_failure: false

apply:
  stage: apply
  # Fun bash features used here:
  # - Evaluating indirect/reference variables: ${!KUBE_ENV}, returns the value of $kube_config_test/prod
  # - Process Substitution: <(echo ${!KUBE_ENV} | base64 -d), output is refered to as a file and is used as input of --kubeconfig parameter
  script:
    - apk add --update curl
    - curl -L https://dl.k8s.io/release/v1.23.0/bin/linux/amd64/kubectl -o /usr/bin/kubectl
    - chmod +x /usr/bin/kubectl
    - KUBE_ENV="kube_config_${PARENT_STAGE}"
    - TLS_ENV="tls_${PARENT_STAGE}"
    # Restore any exisitng stage specific TLS certificate resources, stored as base64 encoded, masked Gitlab-ci variables. Removing original uid and resourceVersion
    - if [ ! -z ${!TLS_ENV} ]; then echo "Restoring ${TLS_ENV}" && kubectl apply -f <(echo ${!TLS_ENV} | base64 -d | grep "uid\|resourceVersion" -v) --kubeconfig <(echo ${!KUBE_ENV} | base64 -d); fi
    - kubectl apply -f deploy/k8s/root.yaml --kubeconfig <(echo ${!KUBE_ENV} | base64 -d)

# Prevent the edited ApplicationSet `root.yaml` from being passed between deployments
empty_cache:
  stage: empty_cache
  script:
    - rm -f deploy/k8s/root.yaml
  when: always
