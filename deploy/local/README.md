# Summary

Resources for local deployment. Includes:

- Kind deployment: Full k8s deployment using local workstation
- docker-compose: Deploy mongodb, use when running `/src/api/main.go` directly

# Kind deployment

- Create and source `/deploy/local/.env` using `/deploy/local/example-env` as a reference
- Run `deploy/local/build.sh`. Updating the `dev` container image tags
- Run `/deploy/local/kind.sh`. Deploys the kind resources, bootstraps ArgoCD and adds public DNS records in CloudFlare, with private IPv4 values to local services
- Access ArgoCD on [https://localhost:8080](https://localhost:8080). Username: `admin`, Password: Final output line of `/deploy/local/kind.sh`
- Access Golang API component at [https://api.dev.laterail.com/](https://api.dev.laterail.com/), React component at [https://dev.laterail.com/](https://dev.laterail.com/)
- Destroy kind cluster with `kind delete cluster`

Notes:

- `/deploy/local/kind.sh` will replace values in `deploy/k8s/root.yaml`, just like the gitlab-ci managed deployments. Don't commit these changes to the repo.

## Quick reference

```
#PWD=deploy/local
set -a && . .env && set +a
./build.sh
./kind.sh
```

# Docker compose deployment

- Create and source `/deploy/local/.env` using `/deploy/local/example-env` as a reference
  - Special consideration to service URLs which will have different DNS addresses (equivalent to container name) in the docker compose network
- Ensure required paths exist for volume mounts
- Deploy with `docker-compose up -d`. Will create and seed a mongodb instance
- Run `src/api/main.go`
- Destroy with `docker-compose up -d`. Will not remove volume mount path resources

# Local AgroCD

By default ArgoCD Application Sets are configured to continuously pull config from the remote repo origin. Disable this for local development investigating/troubleshooting by:

- set `ApplicationSet.spec.template.spec.syncPolicy.automated.selfHeal` == false in `deploy/k8s/root.yaml`
- use the argocd cli to update applications to sync from file paths local to the cli

Example:

```
#$PWD=./laterail-go/deploy/local
ARGO_PASS=$(kubectl --namespace argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d)
argocd login localhost:8080 --username admin --password $ARGO_PASS --insecure
kubectl -n argocd patch --type='merge' applicationset laterail-core -p "{\"spec\":{\"template\":{\"spec\":{\"syncPolicy\":null}}}}"
kubectl -n argocd patch --type='merge' applicationset laterail-dev -p "{\"spec\":{\"template\":{\"spec\":{\"syncPolicy\":null}}}}"
argocd app diff dev --local ../k8s/kustomize/overlays/dev
argocd app sync dev --local ../k8s/kustomize/overlays/dev
```
