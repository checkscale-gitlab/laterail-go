#!/bin/bash
set -e

REPO_ROOT=$PWD

# Build container images
cd "$REPO_ROOT/../../src/api"
docker build . -t registry.gitlab.com/alexchadwick/laterail-go/api:dev

cd "$REPO_ROOT/../../src/react"
npm install
npm run build
docker build . -t registry.gitlab.com/alexchadwick/laterail-go/web:dev

# Push to gitlab
docker push registry.gitlab.com/alexchadwick/laterail-go/api:dev
docker push registry.gitlab.com/alexchadwick/laterail-go/web:dev