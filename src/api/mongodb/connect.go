package mongodb

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

// Connect to mongodb collection using URI set by env var
func NewClient(url string) (*mongo.Client, error) {

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	clientOpts := options.Client().ApplyURI(url)
	clientOpts.SetAppName("laterail")
	clientOpts.SetMaxPoolSize(uint64(50))

	client, err := mongo.Connect(ctx, clientOpts)
	if err != nil {
		return client, fmt.Errorf("constructing client: %w", err)
	}

	// Test connection
	if err := client.Ping(ctx, readpref.Primary()); err != nil {
		return client, fmt.Errorf("testing client: %w", err)
	}

	return client, nil
}
