package api

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"strconv"
	"testing"
	"time"

	"late-rail-go/db"
	"late-rail-go/mongodb"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/rzajac/zltest"
)

var (
	header = Header{
		FromLocation: "BDG",
		ToLocation:   "GLC",
	}

	serviceAttributesMetrics = ServiceAttributesMetrics{
		OriginLocation:      "LRH",
		DestinationLocation: "DMR",
		GbttPtd:             "1306",
		GbttPta:             "1311",
		TocCode:             "SR",
		MatchedServices:     "1",
		Rids:                []string{"202107308789849"},
	}

	metrics = []Metrics{
		{
			ToleranceValue:   "0",
			NumNotTolerance:  "1",
			NumTolerance:     "0",
			PercentTolerance: "0",
			GlobalTolerance:  true,
		},
		{
			ToleranceValue:   "1",
			NumNotTolerance:  "1",
			NumTolerance:     "0",
			PercentTolerance: "0",
			GlobalTolerance:  false,
		},
	}

	adpScheduledDeparture, _ = NullTime("20210802", "0547")
	adpActualDeparture, _    = NullTime("20210802", "0547")
	adpLateCancelReason, _   = NullInt("")
	adpDelayMins, _          = NullInt("")
	addDetailParams          = []db.AddDetailParams{
		db.AddDetailParams{
			Rid:                202108028789815,
			ServiceID:          8789815,
			StationName:        "MTH",
			ScheduledDeparture: adpScheduledDeparture,
			ScheduledArrival:   sql.NullTime{},
			ActualDeparture:    adpActualDeparture,
			ActualArrival:      sql.NullTime{},
			DelayMins:          adpDelayMins,
			LateCancelReason:   adpLateCancelReason,
		},
	}
	addDetailStations = []db.AddStationParams{
		db.AddStationParams{"MTH", sql.NullString{}},
		db.AddStationParams{"AIR", sql.NullString{}},
		db.AddStationParams{"HNC", sql.NullString{}},
		db.AddStationParams{"HNW", sql.NullString{}},
		db.AddStationParams{"BLT", sql.NullString{}},
		db.AddStationParams{"NTN", sql.NullString{}},
		db.AddStationParams{"CBL", sql.NullString{}},
		db.AddStationParams{"RUT", sql.NullString{}},
		db.AddStationParams{"DAK", sql.NullString{}},
		db.AddStationParams{"BDG", sql.NullString{}},
		db.AddStationParams{"AGS", sql.NullString{}},
		db.AddStationParams{"GLC", sql.NullString{}},
		db.AddStationParams{"AND", sql.NullString{}},
		db.AddStationParams{"EXG", sql.NullString{}},
		db.AddStationParams{"PTK", sql.NullString{}},
		db.AddStationParams{"HYN", sql.NullString{}},
		db.AddStationParams{"ANL", sql.NullString{}},
		db.AddStationParams{"WES", sql.NullString{}},
		db.AddStationParams{"DMC", sql.NullString{}},
		db.AddStationParams{"DMY", sql.NullString{}},
		db.AddStationParams{"SIN", sql.NullString{}},
		db.AddStationParams{"DMR", sql.NullString{}},
	}

	batch = InitialRequest{
		FromLoc:   "BDG",
		ToLoc:     "GLC",
		FromTime:  "0000",
		ToTime:    "1500",
		FromDate:  "2021-07-27",
		ToDate:    "2021-08-06",
		Tolerance: []int{1, 5, 10},
	}

	hspConfig = HspConfig{
		Username: os.Getenv("HSP_USER"),
		Password: os.Getenv("HSP_SECRET"),
	}

	ir    = InitialRequest{}
	mresp = MetricsResponse{}
	dresp = DetailsResponse{}

	dbServer string
)

func init() {
	gin.SetMode(gin.TestMode)
	zerolog.SetGlobalLevel(zerolog.Disabled)
}

func TestMain(m *testing.M) {
	// Import initial requests
	ir = InitialRequest{
		FromLoc:   "BDG",
		ToLoc:     "GLC",
		FromTime:  "0600",
		ToTime:    "2300",
		FromDate:  "2021-06-01",
		ToDate:    "2021-08-31",
		Tolerance: []int{},
	}

	metricsResponseFile, err := os.ReadFile("metricsResponseFile.json")
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("Failed open metrics Response file")
	}
	if err := json.Unmarshal(metricsResponseFile, &mresp); err != nil {
		log.Fatal().
			Err(err).
			Msg("Failed unmarshal metrics Response file")
	}

	detailsResponseFile, err := os.ReadFile("detailsResponseFile.json")
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("Failed open details Response file")
	}
	if err := json.Unmarshal(detailsResponseFile, &dresp); err != nil {
		log.Fatal().
			Err(err).
			Msg("Failed unmarshal details Response file")
	}

	dbServer = setDBServer("DB_SERVER", "localhost")

	//Run tests
	code := m.Run()
	os.Exit(code)
}

func setDBServer(env, defaultValue string) string {
	dbServer, ok := os.LookupEnv(env)
	if !ok {
		dbServer = defaultValue
	}
	return dbServer
}

func performRequest(r http.Handler, method, path string, body io.Reader) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, body)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}

func TestAPIEndpoints(t *testing.T) {
	db.NewTestDatabaseWithConfig(t, dbServer)

	env := Env{
		Stations:  mongodb.StationModel{},
		JwksURL:   "",
		HspConfig: hspConfig,
		LogLevel:  "test",
	}
	router := SetupRouter(env)
	// gin.SetMode(gin.TestMode)

	t.Run("POST /submit returns 200", func(t *testing.T) {
		body := []byte(`{"from_loc":"BDG", "to_loc":"GLC", "from_time":"0600", "to_time":"0630", "from_date":"2021-07-31", "to_date":"2021-07-31", "tolerance":[0, 5, 10]}`)
		w := performRequest(router, "POST", "/submit", bytes.NewBuffer(body))
		if w.Code != http.StatusOK {
			t.Fatalf("got %d, want %d", w.Code, http.StatusOK)
		}
	})

	t.Run("Get /runs returns 200", func(t *testing.T) {
		w := performRequest(router, "GET", "/runs", nil)
		if w.Code != http.StatusOK {
			t.Fatalf("got %d, want %d", w.Code, http.StatusOK)
		}
	})

	//	t.Run("Returns expected body", func(t *testing.T) {
	//		var response []data.InsertRow
	//		w := performRequest(router, "GET", "/runs", nil)
	//		if err := json.Unmarshal([]byte(w.Body.String()), &response); err != nil {
	//			t.Fatalf("unexpected error unmarshalling response: %q", err)
	//		}
	//		if len(response) == 0 {
	//			t.Fatalf("Response empty")
	//		}
	//
	//		for i, e := range expected {
	//			if e["Rid"] != response[i].Rid {
	//				t.Fatalf("got %v, want %v", e["Rid"], response[i].Rid)
	//			}
	//		}
	//	})
}

//func TestLocalStringToUTC(t *testing.T) {
//	got, err := localStringToUTC("202107301306")
//	want := (time.Date(2021, time.July, 30, 13, 6, 0, 0, time.Local)).UTC()
//	if err != nil {
//		t.Fatalf("Unexpected error: %v", err)
//	}
//	if got != want {
//		t.Fatalf("got %v, want %v", got, want)
//	}
//}

func Test_GetHspResponse(t *testing.T) {
	client := http.Client{}
	metricsReqBody := MetricsRequest{"BDG", "GLC", "0600", "0630", "2021-08-02", "2021-08-04", "WEEKDAY", []int{5, 15, 30}}
	detailsReqBody := DetailsRequest{"202108028789815"}
	ctx := context.Background()

	// Crate zerolog test helper
	tst := zltest.New(t)
	log.Logger = tst.Logger().Level(zerolog.DebugLevel)

	t.Run("Metrics req", func(t *testing.T) {
		got, err := GetHspResponse(&client, hspConfig, metricsReqBody, ctx)
		if err != nil {
			t.Fatalf("Failed metrics request")
		}

		// Confirm got is a MetricResponse before comparison using Type assertion
		var m *MetricsRequest
		if m, ok := got.(*MetricsResponse); ok {
			//gotTyped := got.(*MetricsResponse)
			if !reflect.DeepEqual(m, &mresp) {
				t.Fatalf("Expected:%v\nGot:%v\n", &mresp, m)
			}
		} else {
			t.Fatalf("failed to assert type MetricResponse")
		}
		_ = m
	})

	t.Run("Details req", func(t *testing.T) {
		got, err := GetHspResponse(&client, hspConfig, detailsReqBody, ctx)
		if err != nil {
			t.Fatalf("Failed metrics request")
		}

		// Confirm got is a DetailsResponse before comparison using Type assertion
		var m *DetailsResponse
		if m, ok := got.(*DetailsResponse); ok {
			if !reflect.DeepEqual(m, &dresp) {
				t.Fatalf("Expected:%v\nGot:%v\n", &dresp, m)
			}
		} else {
			t.Fatalf("failed to assert type MetricResponse")
		}
		_ = m
	})

	// Test error handling for http 400 response code
	// BROKEN by util.ErrorWithTrace
	t.Run("http 400", func(t *testing.T) {
		_, err := GetHspResponse(&client, hspConfig, MetricsRequest{}, ctx)
		if err == nil {
			t.Fatal("Expected an error")
		}

		// 	// type assertion: checking if `err` is type `BadStatusError`
		// 	got, ok := err.(BadStatusError)
		// 	if !ok {
		// 		t.Fatalf("was not a BadStatusError, got %T", got)
		// 	}

		// want := BadStatusError{400}
		// if got != want {
		// 	t.Fatalf("got %v, want %v", got, want)
		// }
	})

	// // Test logging for responses which contain no Service info
	// t.Run("Empty Services struct response", func(t *testing.T) {
	// 	reqBody := MetricsRequest{"BDG", "GLC", "1300", "1315", "2021-05-30", "2021-05-30", "SUNDAY", []int{1, 5, 10}}
	// 	// buffer := bytes.Buffer{}
	// 	// log.SetOutput(&buffer)
	// 	hspConfig := HspConfig{
	// 		Username: os.Getenv("HSP_USER"),
	// 		Password: os.Getenv("HSP_SECRET"),
	// 	}
	// 	a := hspConfig
	// 	test, _ := GetHspResponse(&client, hspConfig, reqBody, ctx)
	// 	_, _ = test, a

	// 	// got := buffer.String()
	// 	// want := "api.MetricsRequest: {FromLoc:BDG ToLoc:GLC FromTime:1300 ToTime:1315 FromDate:2021-05-30 ToDate:2021-05-30 Days:SUNDAY Tolerance:[1 5 10]}, returned 0 services\n"
	// 	// if got[20:] != want {
	// 	// 	t.Errorf("got %q\nwant %q\n", got[20:], want)
	// 	// }

	// 	got := tst.LastEntry()
	// 	got.ExpNum("returned_services", 0)
	// })
}

func TestGetDayGroup(t *testing.T) {

	t.Run("Sunday", func(t *testing.T) {
		dateTime := time.Date(2021, time.August, 1, 0, 0, 0, 0, time.UTC)
		want := Sunday
		got := getDayGroup(dateTime)

		if got != want {
			t.Fatalf("got %v, want %v", got, want)
		}
	})
}

func TestInitialRequest_TimeRange(t *testing.T) {

	w, _ := time.Parse(timeRefTimeRange, ir.FromTime)
	last, _ := time.Parse(timeRefTimeRange, ir.ToTime)
	want := []time.Time{
		w,
		w.Add(6 * time.Hour),
		w.Add(12 * time.Hour),
		last,
	}
	got, err := ir.TimeRange()
	if err != nil {
		t.Fatalf("failed to create TimeRange slice: %v", err)
	}

	for i := 0; i < len(want); i++ {
		if !reflect.DeepEqual(got[i], want[i]) {
			t.Errorf("InitialRequest.TimeRange(), index %d = %v, want %v", i, got[i], want[i])
		}
	}
}

func TestInitialRequest_DateRange(t *testing.T) {

	w, _ := time.Parse(timeRefDateRange, ir.FromDate)
	last, _ := time.Parse(timeRefDateRange, ir.ToDate)
	want := []time.Time{
		w,
		w.Add(irDateRangeMax),
		w.Add(2 * irDateRangeMax),
		w.Add(3 * irDateRangeMax),
		w.Add(4 * irDateRangeMax),
		w.Add(5 * irDateRangeMax),
		w.Add(6 * irDateRangeMax),
		last,
	}
	got, err := ir.DateRange()
	if err != nil {
		t.Fatalf("failed to create DateRange slice: %v", err)
	}

	for i := 0; i < len(want); i++ {
		if !reflect.DeepEqual(got[i], want[i]) {
			t.Errorf("InitialRequest.DateRange(), index %d = %v, want %v", i, got[i], want[i])
		}
	}
}

func Test_sliceTimeToString(t *testing.T) {

	t.Run("DateRange", func(t *testing.T) {
		timeSlice, err := ir.DateRange()
		if err != nil {
			t.Fatalf("failed to create TimeRange slice: %v", err)
		}
		want := []string{
			"2021-06-01",
			"2021-06-15",
			"2021-06-29",
			"2021-07-13",
			"2021-07-27",
			"2021-08-10",
			"2021-08-24",
			"2021-08-31",
		}
		got := sliceTimeToString(timeSlice, timeRefDateRange)

		for i := 0; i < len(want); i++ {
			if got[i] != want[i] {
				t.Errorf("Index %d, got = %v, want %v", i, got[i], want[i])
			}
		}
	})

	t.Run("TimeRange", func(t *testing.T) {
		timeSlice, err := ir.TimeRange()
		if err != nil {
			t.Fatalf("failed to create TimeRange slice: %v", err)
		}
		want := []string{
			"0600",
			"1200",
			"1800",
			"2300",
		}
		got := sliceTimeToString(timeSlice, timeRefTimeRange)

		for i := 0; i < len(want); i++ {
			if got[i] != want[i] {
				t.Errorf("Index %d, got = %v, want %v", i, got[i], want[i])
			}
		}
	})

}

func Test_getDayTypes(t *testing.T) {
	timeSlice, err := ir.DateRange()
	if err != nil {
		t.Fatalf("failed to create TimeRange slice: %v", err)
	}
	want := []string{"SATURDAY", "SUNDAY", "WEEKDAY"}

	got, err := getDayTypes(timeSlice[0], timeSlice[1])
	if err != nil {
		t.Fatalf("failed to evaluate day types: %v", err)
	}

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got = %v, want %v", got, want)
	}

}

func Test_Split(t *testing.T) {

	ctx := context.Background()
	t.Run("Large", func(t *testing.T) {
		want := []MetricsRequest{
			{"BDG", "GLC", "0600", "1200", "2021-06-01", "2021-06-15", "SATURDAY", metricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-06-01", "2021-06-15", "SUNDAY", metricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-06-01", "2021-06-15", "WEEKDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-06-01", "2021-06-15", "SATURDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-06-01", "2021-06-15", "SUNDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-06-01", "2021-06-15", "WEEKDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-06-01", "2021-06-15", "SATURDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-06-01", "2021-06-15", "SUNDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-06-01", "2021-06-15", "WEEKDAY", metricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-06-15", "2021-06-29", "SATURDAY", metricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-06-15", "2021-06-29", "SUNDAY", metricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-06-15", "2021-06-29", "WEEKDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-06-15", "2021-06-29", "SATURDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-06-15", "2021-06-29", "SUNDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-06-15", "2021-06-29", "WEEKDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-06-15", "2021-06-29", "SATURDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-06-15", "2021-06-29", "SUNDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-06-15", "2021-06-29", "WEEKDAY", metricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-06-29", "2021-07-13", "SATURDAY", metricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-06-29", "2021-07-13", "SUNDAY", metricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-06-29", "2021-07-13", "WEEKDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-06-29", "2021-07-13", "SATURDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-06-29", "2021-07-13", "SUNDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-06-29", "2021-07-13", "WEEKDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-06-29", "2021-07-13", "SATURDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-06-29", "2021-07-13", "SUNDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-06-29", "2021-07-13", "WEEKDAY", metricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-07-13", "2021-07-27", "SATURDAY", metricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-07-13", "2021-07-27", "SUNDAY", metricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-07-13", "2021-07-27", "WEEKDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-07-13", "2021-07-27", "SATURDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-07-13", "2021-07-27", "SUNDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-07-13", "2021-07-27", "WEEKDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-07-13", "2021-07-27", "SATURDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-07-13", "2021-07-27", "SUNDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-07-13", "2021-07-27", "WEEKDAY", metricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-07-27", "2021-08-10", "SATURDAY", metricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-07-27", "2021-08-10", "SUNDAY", metricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-07-27", "2021-08-10", "WEEKDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-07-27", "2021-08-10", "SATURDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-07-27", "2021-08-10", "SUNDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-07-27", "2021-08-10", "WEEKDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-07-27", "2021-08-10", "SATURDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-07-27", "2021-08-10", "SUNDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-07-27", "2021-08-10", "WEEKDAY", metricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-08-10", "2021-08-24", "SATURDAY", metricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-08-10", "2021-08-24", "SUNDAY", metricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-08-10", "2021-08-24", "WEEKDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-08-10", "2021-08-24", "SATURDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-08-10", "2021-08-24", "SUNDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-08-10", "2021-08-24", "WEEKDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-08-10", "2021-08-24", "SATURDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-08-10", "2021-08-24", "SUNDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-08-10", "2021-08-24", "WEEKDAY", metricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-08-24", "2021-08-31", "SATURDAY", metricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-08-24", "2021-08-31", "SUNDAY", metricsRequestTolerance},
			{"BDG", "GLC", "0600", "1200", "2021-08-24", "2021-08-31", "WEEKDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-08-24", "2021-08-31", "SATURDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-08-24", "2021-08-31", "SUNDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1200", "1800", "2021-08-24", "2021-08-31", "WEEKDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-08-24", "2021-08-31", "SATURDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-08-24", "2021-08-31", "SUNDAY", metricsRequestTolerance},
			{"BDG", "GLC", "1800", "2300", "2021-08-24", "2021-08-31", "WEEKDAY", metricsRequestTolerance},
		}
		got, err := ir.Split(ctx)
		if err != nil {
			t.Fatalf("failed to splitting InitialRequest: %v", err)
		}

		for i := 0; i < len(want); i++ {
			if !reflect.DeepEqual(got[i], want[i]) {
				t.Errorf("InitialRequest.TimeRange(), index %d, got = %v, want %v", i, got[i], want[i])
			}
		}
	})

	t.Run("Small", func(t *testing.T) {
		want := []MetricsRequest{
			{"BDG", "GLC", "0600", "0630", "2021-07-31", "2021-07-31", "SATURDAY", metricsRequestTolerance},
		}
		ir := InitialRequest{"BDG", "GLC", "0600", "0630", "2021-07-31", "2021-07-31", []int{1, 5, 10}}
		got, err := ir.Split(ctx)
		if err != nil {
			t.Fatalf("failed to splitting InitialRequest: %v", err)
		}

		for i := 0; i < len(want); i++ {
			if !reflect.DeepEqual(got[i], want[i]) {
				t.Errorf("InitialRequest.TimeRange(), index %d, got = %v, want %v", i, got[i], want[i])
			}
		}
	})
}

func Test_MetricsResponse_Station(t *testing.T) {
	want := []db.AddStationParams{
		{
			StationID:   "BDG",
			StationName: sql.NullString{"", false},
		},
		{
			StationID:   "GLC",
			StationName: sql.NullString{"", false},
		},
		{
			StationID:   "MTH",
			StationName: sql.NullString{"", false},
		},
		{
			StationID:   "DMR",
			StationName: sql.NullString{"", false},
		},
	}

	got := mresp.Station()

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got = %v, want %v", got, want)
	}
}

func Test_MetricsResponse_Service(t *testing.T) {

	t.Run("Service", func(t *testing.T) {
		want := []db.AddServiceParams{
			{
				ServiceID:          8789815,
				StationOrigin:      "MTH",
				StationDestination: "DMR",
			},
			{
				ServiceID:          8789923,
				StationOrigin:      "MTH",
				StationDestination: "DMR",
			},
		}
		got, _, err := mresp.Service()
		if err != nil {
			t.Fatalf("failed to : %v", err)
		}

		if !reflect.DeepEqual(got, want) {
			t.Errorf("\ngot = %v,\nwant = %v", got, want)
		}
	})

	t.Run("Run", func(t *testing.T) {
		want := []db.AddRunParams{
			{
				Rid:       202108028789815,
				ServiceID: 8789815,
			},
			{
				Rid:       202108028789923,
				ServiceID: 8789923,
			},
		}
		_, got, err := mresp.Service()
		if err != nil {
			t.Fatalf("failed to : %v", err)
		}

		if !reflect.DeepEqual(got, want) {
			t.Errorf("\ngot = %v,\nwant = %v", got, want)
		}
	})
}

func Test_PostMetrics(t *testing.T) {
	ctx := context.Background()
	db.NewTestDatabaseWithConfig(t, dbServer)
	initial := InitialRequest{"BDG", "GLC", "0600", "0630", "2021-08-02", "2021-08-03", []int{1, 5, 10}}

	ridSlice, err := PostMetrics(initial, GetHspResponse, hspConfig, ctx)
	if err != nil {
		t.Fatalf("Failed to process new request: %v", err)
	}

	t.Run("Expected number of stations", func(t *testing.T) {
		n := 4
		got, err := db.New(db.Conn).ListStations(ctx)
		if err != nil {
			t.Fatalf("failed to get stations from db: %s", err)
		}

		i := len(got)
		if i != n {
			t.Fatalf("Got %d stations from database. Expected %d", i, n)
		}
	})

	t.Run("Expected number of services", func(t *testing.T) {
		n := 2
		got, err := db.New(db.Conn).ListServices(ctx)
		if err != nil {
			t.Fatalf("failed to get services from db: %s", err)
		}

		i := len(got)
		if i != n {
			t.Fatalf("Got %d services from database. Expected %d", i, n)
		}
	})

	t.Run("Expected number of runs", func(t *testing.T) {
		n := 2
		got, err := db.New(db.Conn).ListRuns(ctx)
		if err != nil {
			t.Fatalf("failed to get runs from db: %s", err)
		}

		i := len(got)
		if i != n {
			t.Fatalf("Got %d runs from database. Expected %d", i, n)
		}
	})

	t.Run("Expected output RID slice", func(t *testing.T) {
		want := []int64{202108028789815, 202108028789923}

		if !reflect.DeepEqual(ridSlice, want) {
			t.Fatalf("Expected %v, got %v", want, ridSlice)
		}
	})

	// Broken by use of util.ErrorWithTrace
	t.Run("Expected zero returned services", func(t *testing.T) {
		initial = InitialRequest{"BDG", "GLQ", "0600", "0630", "2021-08-02", "2021-08-03", []int{1, 5, 10}}

		got, err := PostMetrics(initial, GetHspResponse, hspConfig, ctx)
		if err == nil {
			t.Fatalf("Expected error")
		}

		// Type assertion
		// got, _ := err.(NoMatchingServicesError)
		// if !isErr {
		// 	t.Fatalf("Expected error of type NoMatchingServicesError, got %T", err)
		// }

		// want := NoMatchingServicesError{InitialRequest: initial}
		// if !reflect.DeepEqual(got, want) {
		// 	t.Fatalf("got %v, want %v", got, want)
		// }
		_ = got

	})
}

func Test_PostDetails(t *testing.T) {
	ctx := context.Background()
	db.NewTestDatabaseWithConfig(t, dbServer)
	rids := []int64{202108028789815}

	// Need to populate db tables to satisfy foreign key constraints
	initial := InitialRequest{"BDG", "GLC", "0600", "0630", "2021-08-02", "2021-08-03", []int{1, 5, 10}}
	_, err := PostMetrics(initial, GetHspResponse, hspConfig, ctx)
	if err != nil {
		t.Fatalf("Failed to process new request")
	}

	_, err = PostDetails(rids, GetHspResponse, hspConfig, ctx)
	if err != nil {
		t.Fatalf("Failed to process new request")
	}

	t.Run("Expected number of details", func(t *testing.T) {
		n := 22
		got, err := db.New(db.Conn).ListDetails(ctx)
		if err != nil {
			t.Fatalf("failed to get details from db: %v", err)
		}

		i := len(got)
		if i != n {
			t.Fatalf("Got %d details from database. Expected %d", i, n)
		}
	})
}

func TestAPIPost(t *testing.T) {

	ctx := context.Background()
	db.NewTestDatabaseWithConfig(t, dbServer)

	env := Env{
		Stations:  mongodb.StationModel{},
		JwksURL:   "",
		HspConfig: hspConfig,
		LogLevel:  "test",
	}
	router := SetupRouter(env)
	// gin.SetMode(gin.TestMode)

	t.Run("POST /submit returns expected number of stations", func(t *testing.T) {
		body := []byte(`{"from_loc":"BDG", "to_loc":"GLC", "from_time":"0600", "to_time":"0630", "from_date":"2021-08-02", "to_date":"2021-08-04", "tolerance":[0, 5, 10]}`)
		w := performRequest(router, "POST", "/submit", bytes.NewBuffer(body))
		if w.Code != http.StatusOK {
			t.Fatalf("got %d, want %d", w.Code, http.StatusOK)
		}

		n := 33
		got, err := db.New(db.Conn).ListStations(ctx)
		if err != nil {
			t.Fatalf("failed to get stations from db: %s", err)
		}

		i := len(got)
		if i != n {
			t.Fatalf("Got %d stations from database. Expected %d", i, n)
		}
	})
}

func Test_NullTime(t *testing.T) {
	rid := "202108028789815"
	ridString := rid[:8]

	t.Run("valid", func(t *testing.T) {
		timeString := "0547"
		got, err := NullTime(ridString, timeString)
		if err != nil {
			t.Fatalf("failed to parse sql nulltime")
		}
		timeParsed, _ := time.Parse(timeRefDatetime, ridString+timeString)

		want := sql.NullTime{timeParsed, true}
		if !reflect.DeepEqual(got, want) {
			t.Fatalf("Got %+v. Expected %+v", got, want)
		}
	})

	t.Run("empty", func(t *testing.T) {
		timeString := ""
		got, err := NullTime(ridString, timeString)
		if err != nil {
			t.Fatalf("failed to parse sql nulltime")
		}

		var want sql.NullTime
		if !reflect.DeepEqual(got, want) {
			t.Fatalf("Got %+v. Expected %+v", got, want)
		}
	})

	t.Run("invalid", func(t *testing.T) {
		timeString := "abc"
		_, err := NullTime(ridString, timeString)
		if err == nil {
			t.Fatalf("Expected error")
		}
	})
}

func Test_NullInt(t *testing.T) {
	t.Run("valid", func(t *testing.T) {
		intString := "10"
		got, err := NullInt(intString)
		if err != nil {
			t.Fatalf("failed to parse sql nullint")
		}
		i, _ := strconv.Atoi(intString)
		i32 := int32(i)

		want := sql.NullInt32{i32, true}
		if !reflect.DeepEqual(got, want) {
			t.Fatalf("Got %+v. Expected %+v", got, want)
		}
	})

	t.Run("empty", func(t *testing.T) {
		intString := ""
		got, err := NullInt(intString)
		if err != nil {
			t.Fatalf("failed to parse sql nullint")
		}

		want := sql.NullInt32{}
		if !reflect.DeepEqual(got, want) {
			t.Fatalf("Got %+v. Expected %+v", got, want)
		}
	})

	t.Run("invalid", func(t *testing.T) {
		intString := "abc"
		_, err := NullInt(intString)
		if err == nil {
			t.Fatalf("Expected error")
		}
	})
}

func TestDetailsResponse_Location(t *testing.T) {

	// Use different details example
	dr := DetailsResponse{}
	detailsResponseFile, err := os.ReadFile("detailsResponseFile-12.json")
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("Failed open details Response file")
	}
	if err := json.Unmarshal(detailsResponseFile, &dr); err != nil {
		log.Fatal().
			Err(err).
			Msg("Failed unmarshal details Response file")
	}

	// Where stop has an actual_arrival and isn't the starting station
	t.Run("default", func(t *testing.T) {

		adpScheduledDeparture, _ = NullTime("20211115", "0625")
		adpActualDeparture, _ = NullTime("20211115", "0624")
		delayMins, _ := NullInt("-1")
		addDetailParams = []db.AddDetailParams{
			db.AddDetailParams{
				Rid:                202111158791435,
				ServiceID:          8791435,
				StationName:        "GLQ",
				ScheduledDeparture: adpScheduledDeparture,
				ScheduledArrival:   sql.NullTime{},
				ActualDeparture:    adpActualDeparture,
				ActualArrival:      sql.NullTime{},
				DelayMins:          delayMins,
				LateCancelReason:   sql.NullInt32{},
			},
		}

		want := addDetailParams

		got, err := dr.Location()
		if err != nil {
			t.Fatalf("failed to parse details repsonse: %q", err)
			return
		}
		if !reflect.DeepEqual(got[0], want[0]) {
			t.Errorf("got: %v\nwant %v", got[0], want[0])
		}

	})

	// The first stop will have no scheduled arrival time
	// Delays should be calculated using departure times
	t.Run("departure delay", func(t *testing.T) {

		adpScheduledDeparture, _ = NullTime("20211115", "0625")
		adpActualDeparture, _ = NullTime("20211115", "0624")
		delayMins, _ := NullInt("-1")
		addDetailParams = []db.AddDetailParams{
			db.AddDetailParams{
				Rid:                202111158791435,
				ServiceID:          8791435,
				StationName:        "GLQ",
				ScheduledDeparture: adpScheduledDeparture,
				ScheduledArrival:   sql.NullTime{},
				ActualDeparture:    adpActualDeparture,
				ActualArrival:      sql.NullTime{},
				DelayMins:          delayMins,
				LateCancelReason:   sql.NullInt32{},
			},
		}

		want := addDetailParams

		got, err := dr.Location()
		if err != nil {
			t.Fatalf("failed to parse details repsonse: %q", err)
			return
		}
		if !reflect.DeepEqual(got[0], want[0]) {
			t.Errorf("got: %v\nwant %v", got[0], want[0])
		}

	})

	// Where stop has no actual_arrival value. Skipped
	t.Run("delay value error:-153722867", func(t *testing.T) {

		adpScheduledDeparture, _ = NullTime("20211115", "0643")
		adpActualDeparture, _ = NullTime("20211115", "0643")
		adpScheduledArrival, _ := NullTime("20211115", "0643")
		adpLateCancelReason, _ = NullInt("")
		addDetailParams = []db.AddDetailParams{
			db.AddDetailParams{
				Rid:                202111158791435,
				ServiceID:          8791435,
				StationName:        "GRH",
				ScheduledDeparture: adpScheduledDeparture,
				ScheduledArrival:   adpScheduledArrival,
				ActualDeparture:    adpActualDeparture,
				ActualArrival:      sql.NullTime{},
				DelayMins:          sql.NullInt32{}, //-153722867,
				LateCancelReason:   adpLateCancelReason,
			},
		}

		want := addDetailParams

		got, err := dr.Location()
		if err != nil {
			t.Fatalf("failed to parse details repsonse: %q", err)
			return
		}
		if !reflect.DeepEqual(got[4], want[0]) {
			t.Errorf("got: %v\nwant %v", got[4], want[0])
		}

	})

}

func Test_DetailsResponse_Station(t *testing.T) {
	want := addDetailStations
	got := dresp.Station()

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got: %v\nwant %v", got, want)
	}
}

func Test_NewDetails(t *testing.T) {
	ctx := context.Background()

	db.NewTestDatabaseWithConfig(t, dbServer)

	env := Env{
		Stations:  mongodb.StationModel{},
		JwksURL:   "",
		HspConfig: hspConfig,
		LogLevel:  "test",
	}
	router := SetupRouter(env)
	// gin.SetMode(gin.TestMode)

	// Search which returns the rids: 202111068789823, 202111068789871, 202111068789937, 202111068789939, 202111068793510, 202111068793955
	body := []byte(`{"from_loc":"BDG", "to_loc":"GLC", "from_time":"1000", "to_time":"1100", "from_date":"2021-11-06", "to_date":"2021-11-06", "tolerance":[0, 5, 10]}`)
	w := performRequest(router, "POST", "/submit", bytes.NewBuffer(body))
	if w.Code != http.StatusOK {
		t.Fatalf("API request failed: http %d", w.Code)
	}

	// Compare the contents of the database with a list of rid values. Returning values missing
	rids := []int64{202111068789822, 202111068789823, 202111068789871, 202111068789872, 202111068789937, 202111068789939, 202111068793510, 202111068793955, 202111068793956}
	ridsNew, err := NewDetails(rids, ctx)
	if err != nil {
		t.Fatalf("Failed to process new rid array: %v", err)
	}

	t.Run("Expected new rids array", func(t *testing.T) {
		want := []int64{202111068789822, 202111068789872, 202111068793956}
		if !reflect.DeepEqual(ridsNew, want) {
			t.Fatalf("Got %+v. Expected %+v", ridsNew, want)
		}
	})
}

// Bench results from before and after goroutines, unit duration of operation == ns
func BenchmarkPostServices(b *testing.B) {
	ctx := context.Background()
	db.NewTestDatabaseWithConfig(b, dbServer)
	initial := InitialRequest{"BDG", "GLC", "0600", "0630", "2021-08-02", "2021-08-04", []int{1, 5, 10}}

	if _, err := PostMetrics(initial, GetHspResponse, hspConfig, ctx); err != nil {
		b.Fatalf("Failed to process new request")
	}
}
