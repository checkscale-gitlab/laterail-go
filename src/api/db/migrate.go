package db

import (
	"errors"
	"fmt"
	"os"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/cockroachdb"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

// Runs database migrations which target a db URL
func Migrate(url string) error {

	// migration files
	var migrationDir string
	if _, err := os.Stat("./db/migrations/"); !os.IsNotExist(err) {
		migrationDir = "file://./db/migrations/"
	} else if _, err := os.Stat("../db/migrations/"); !os.IsNotExist(err) {
		migrationDir = "file://../db/migrations/"
	} else if _, err := os.Stat("/migrations/"); !os.IsNotExist(err) {
		migrationDir = "file:///migrations/"
	} else {
		return fmt.Errorf("migration directory not found: %w", err)
	}

	m, err := migrate.New(migrationDir, url)
	if err != nil {
		return fmt.Errorf("failed create migrate: %w", err)
	}

	if err := m.Up(); err != nil && !errors.Is(err, migrate.ErrNoChange) {
		return fmt.Errorf("failed run migrate: %w", err)
	}

	srcErr, dbErr := m.Close()
	if srcErr != nil {
		return fmt.Errorf("migrate source error: %w", srcErr)
	}
	if dbErr != nil {
		return fmt.Errorf("migrate database error: %w", dbErr)
	}

	return nil
}
