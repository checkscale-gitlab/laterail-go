[![pipelines](https://gitlab.com/alexchadwick/laterail-go/badges/main/pipeline.svg?ignore_skipped=true&key_text=pipelines)](https://gitlab.com/alexchadwick/laterail-go/-/pipelines)

# Aim

This project is a testing ground for devops tools, processes and general software development experience.

The app pulls National Rail UK train journey data from official APIs, calculates delays/cancellations, and reports when you're eligble to claim money back via the rail service delay repay schemes.

# Demo

[Here: laterail.com](https://laterail.com)

The full project is only deployed when I'm actively working on it but the React frontend can also be deployed as a standalone static site demo on CloudFlares Pages.

Gitlab pipelines ensure the demo site is running when the full project isn't.

# Progress

- **Backend API**: Grabs journey info for UK train services and processes to determine delays/cancellations
- **Frontend**: Search feature for backend API requests. Displays results with some stats
- **Fully automatic**: Test, build, provision, deploy and destroy process using Gitlab CI/CD pipelines
- **Observability**: Export linked distributed logs and traces from the API and Kubernetes resources

# Tools

## Using

- **Golang backend**: API which processes journey data fetched from the [Historical Service Performance (HSP) Darwin API](https://wiki.openraildata.com/index.php/HSP)
- **React frontend**: Single Page App using Material-UI components and Auth0 for user authentication
- **Kubernetes**: Using Azure Kubernetes Service and kind (local development)
  - Incl. cert-manager, ingress-nginx, external-dns, external-secrets, oauth2-proxy
- **Observability**:
  - Traces - OpenTelemetry Collector exporting to Jaeger (in-cluster) and Tempo (Grafana Cloud)
  - Logs (linked to traces) - Loki (Grafana Cloud) via Promtail
  - Metrics - OpenTelemetry Collector exporting to Prometheus (Grafana Cloud)
- **ArgoCD**: Deploy and manage Kubernetes resources with a GitOps approach
- **Terraform**: Provision/destroy infrastructure, and bootstrap Kubernetes cluster with ArgoCD
- **Gitlab CI/CD**:
  - Build app service container images via a multi-stage build process
  - Provision and destroy public cloud resources using Terraform
  - Bootstrap new ArgoCD clusters and deploy ApplicationSets
  - Manage public DNS changes for resources which aren't managed by Kubernetes external-dns
- **Databases**:
  - PostgreSQL/CockroachDB serverless
  - MongoDB
- **Auth0**: User identity service (IDaaS), oidc, oauth

## Wanted/Planned

- Add more metrics. Currenly only has a single app metric to investigate the process
- Security scanning:
  - SAST: [gosec](https://github.com/securego/gosec), [semgrep](https://semgrep.dev/)
  - DAST: [zaproxy](https://www.zaproxy.org/)
  - IaC: [KICS](https://kics.io/), [tfsec](https://aquasecurity.github.io/tfsec)
- Feature Flags: [Gitlab's Unleash service](https://docs.gitlab.com/ee/operations/feature_flags.html)
- Break Golang backend into microservices where possible/appropriate:
  - API Gateway features filled by ingress-nginx (north-south) and a service mesh (east-west)
  - Use mixture of containers and FaaS tools, e.g. [OpenFaas](https://docs.openfaas.com)
  - Event streaming w/ Kafka ([Strimzi](https://strimzi.io/))
- Migrate to AWS, replacing Azure
  - Use EKS
  - Inlc. networking improvements e.g. PrivateLink/VPC Peering for CockroachDB ([not available for serverless - 20220515](https://www.cockroachlabs.com/docs/stable/security-reference/security-overview.html#comparison-of-security-features)) and MongoDB Atlas (replacing K8s hosted containers)
- A dependency update bot for pipelines e.g. [Renovate](https://github.com/renovatebot/renovate)
- [Grafana](https://grafana.com/): Dashboards config for OpenTelemetry data
